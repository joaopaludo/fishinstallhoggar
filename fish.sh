#!/bin/bash

# Script for installing Fish Shell on systems without root access.
# Fish Shell will be installed in $HOME/local/bin.
# It's assumed that wget and a C/C++ compiler are installed.

# exit on error
set -e

FISH_SHELL_VERSION=3.0.2
FISH_CONFIG_DIR="$HOME/.config/fish/"


# extract files, configure, and compile
tar xvzf fish-${FISH_SHELL_VERSION}.tar.gz
cd fish-${FISH_SHELL_VERSION}
./configure --prefix=$HOME/local --disable-shared
make
make install

#Add fish dir to PATH
echo "\nPATH=\$PATH:\$HOME/local/bin" >> $HOME/.zshrc


#Adiciona alias da hoggar no fish
echo "alias win 'zsh /etc/zsh/zservcpu'"    >> $FISH_CONFIG_DIR/fish_aliases
echo "alias carga 'zsh /etc/zsh/zcargacpu'" >> $FISH_CONFIG_DIR/fish_aliases
echo ""                                     >> $FISH_CONFIG_DIR/fish_aliases

#Adiciona alias do zshrc no fish
cat $HOME/.zshrc | grep alias | sed 's/=/ /g' >> $FISH_CONFIG_DIR/fish_aliases


#Cria dir functions
[ -d $FISH_CONFIG_DIR/functions ] || mkdir $FISH_CONFIG_DIR/functions


#Adiciona mensagem de saudação
echo "function fish_greeting
    echo (set_color red)'***--- Acessando Servidora HOGGAR ---***'(set_color white)
        echo ''
        ls
end
" > $FISH_CONFIG_DIR/functions/fish_greeting.fish


#Configura prompt
echo "function fish_prompt
        test $SSH_TTY
    and printf (set_color red)(prompt_hostname)(set_color white)'|'(set_color blue)$USER' '
    #and printf (set_color blue)$USER(set_color brwhite)'@'(set_color red)(prompt_hostname)' '
    test "$USER" = 'root'
    and echo (set_color red)"#"

    # Main
    echo -n (set_color yellow)(prompt_pwd) (set_color white)'>'(set_color white)' '
end
" > $FISH_CONFIG_DIR/functions/fish_prompt.fish


